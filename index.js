//console.log("Hello, B204!");

/*
	Objects
		-An object is a data type that is used to represent a real world object
		-It is a collection of related data and/or functionalities
		-Information is stored in object represented in "key: value" pair
			key -> property of the object
			value -> actual data to be stored
		- Different data types may be stored in an object's property creating complex data structures

		Two ways of creating object in javascript
			1. Object Literal Notation 
				let/const objectName = {}

			2. Object Constructor Notation
				Object Instantiation ( let object = new Object() )
	
	Object Literal Notation
		- This creates/declares an object and also initializes/assigns it's properties upon creation
		- A cellphone is an example of a real world object
		- It has it's own properties such as name, color, weight, unit model and a lot of other things

		Syntax:
			let/const = {
				keyA: value,
				keyB: valueB
			};
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects using literal notation");
console.log(cellphone);

console.log(typeof cellphone);


let cellphone2 = {
	name: "Motorola",
	manufactureDate: 2000
};

console.log(cellphone2);


//Creating objects using constructor function
/*
	- Creates a reusable function to create several objects that have the same data structure
	- This is useful for creating multiple instances/copies of an object
	- An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
	 

	
	Syntax:
		function ObjectName(keyA, keyB) {
			this.keyA = keyA;
			this.keyB = keyB;
		}

*/

// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters

function Laptop(name, manufactureDate) {
	//this.propertyName = value
	this.nameProperty = name;
	this.manufactureDateProperty = manufactureDate;
}

// This is a unique instance of the Laptop object
/*
    - The "new" operator creates an instance of an object
    - Objects and instances are often interchanged because object literals (let object = {}) and instances (let object = new object) are distinct/unique objects
*/

let laptop = new Laptop('Lenovo', 2008);
console.log(laptop);// alert("hello");


let oldLaptop = new Laptop('IBM', 1980);
console.log(oldLaptop);

let myLaptop = new Laptop('Macbook', 2020);
console.log(myLaptop);

console.log(myLaptop.nameProperty);

let array = [laptop, myLaptop];
console.log(array);

//calling using dot notation
//arrayName[indexNumber].propertyName

console.log(array[1].nameProperty); // Macbook

//Initializing/Adding/Deleting/Reassigning Object Properties

let car ={};
console.log(car);

car.name ='Sarao';
console.log ('Result from adding properties using dot notation');
console.log(car);



//adding properties using square bracket allows us to use space between property Name.
car ['manufacture date'] = 2019;
console.log(car);

//delete Property

delete car ['manufacture date'];
console.log(car);

car.manufactureDate = 2019;
console.log(car);

//Reassigning

car.name = 'Mustang';
console.log(car);

// Object Methods


let person = {
	//properti
	name: 'Jack',

	//method
	talk: function() {
		console.log ('Hello my name is ' +this.name);
	}
}

console.log(person);
person.talk();

person.walk = function(){
	console.log(this.name + ' walked 25 steps forward');
}
person.walk();

//Methods are useful for creating reusable functions that performs tasks

let friend = {
	firstName: 'Rafael',
	lastName: 'Santillan',
	address:{
		city: 'Quezon City',
		country: 'Philippines'
	},
	email: ['raf@mail.com', 'raf123@yahoo.com'],

	introduce: function(){
		console.log('Hello My name is ' +this.firstName + ' ' +this.lastName);
	}
}

friend.introduce();

//Real world application of objects

// let myPokemon ={
// 	name = 'Pikachu',
// 	level = 3,
// 	health = 100,
// 	attack = 50,

// 	tackle: function(){
// 		console.log('This pokemon tackle target pokemon');
// 	}
// }


function Pokemon(name, level, health){
	//properties
	this.name =name;
	this.level = level;
	this.health = health;
	this.attack = level;

	//methods

	this.tackle = function(target){
		console.log(this.name + ' tackled ' +target.name);
		console.log(target.name + " 's health is now reduced to " +(target.health - this.attack));

		target.health = (target.health-this.attack);

		if (target.health<=0){
			target.faint();
		}

		

	},
	this.faint = function(){
		console.log(this.name + " fainted");
	}
}

let Squirtle = new Pokemon ('Squirtle', 99, 200) ;
let snorlax = new Pokemon ('Snorlax', 75, 500) ;

Squirtle.tackle(snorlax);
Squirtle.tackle(snorlax);
Squirtle.tackle(snorlax);
Squirtle.tackle(snorlax);
Squirtle.tackle(snorlax);
Squirtle.tackle(snorlax);

